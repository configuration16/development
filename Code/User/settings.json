{
    "editor.smoothScrolling": true,
    "editor.bracketPairColorization.independentColorPoolPerBracketType": true,
    "workbench.preferredDarkColorTheme": "Tokyo Night",
    "workbench.colorTheme": "Tokyo Night Storm",
    "explorer.fileNesting.enabled": true,
    "explorer.fileNesting.expand": false,
    "explorer.fileNesting.patterns": {
        "*.ts": "${capture}.test.ts",
        "*.test.ts": "${capture}.test.ts",
        "*.test.tsx": "${capture}.test.tsx"
    },
    "explorer.openEditors.visible": 0,
    "terminal.integrated.commandsToSkipShell": [
        "workbench.action.createTerminalEditor"
    ],
    // GitLens
    "gitlens.hovers.currentLine.enabled": false,
    // Git
    "git.enableCommitSigning": true,
    "git.useEditorAsCommitInput": false,
    "git.useIntegratedAskPass": true,
    "github.gitAuthentication": true,
    // Vim
    "vim.camelCaseMotion.enable": true,
    "vim.smartRelativeLine": true,
    "vim.easymotion": true,
    "vim.leader": "<space>",
    "vim.useSystemClipboard": true,
    "vim.surround": true,
    "vim.handleKeys": {
        "<C-a>": false,
    },
    "vim.highlightedyank.enable": true,
    "vim.ignorecase": false,
    "vim.normalModeKeyBindingsNonRecursive": [
        // Yank until end of line with Y
        {
            "before": ["Y"],
            "after": ["y", "$"],
        },
        // Move through folds without expanding (also good for wrapped lines)
        {
            "before": ["k"],
            "after": ["g", "k"],
        },
        {
            "before": ["j"],
            "after": ["g", "j"],
        }
    ],
    "vim.normalModeKeyBindings": [
        // Fix scrolling through folded code
        {
            "before": ["<C-d>"],
            "after": ["1", "5", "g", "j", "z", "z"]
        },
        {
            "before": ["<C-u>"],
            "after": ["1", "5", "g", "k", "z", "z"],
        },
        // "Goto references"
        {
            "before": ["g", "r"],
            "commands": ["editor.action.goToReferences"],
        },
        // "Goto preview definitition"
        {
            "before": ["g", "p", "d"],
            "commands": ["editor.action.peekDefinition"],
        },
        /************* Leader Binds ****************/
        // "comment"
        {
            "before": ["<leader>", "/"],
            "commands": ["editor.action.commentLine"],
        },
        // "find" (forward easy motion)
        {
            "before": ["<leader>", "f"],
            "after": ["<leader>", "<leader>", "f"],
        },
        // "FIND" (backward easy motion)
        {
            "before": ["<leader>", "F"],
            "after": ["<leader>", "<leader>", "F"],
        },
        // "search filename" (Quick open file by name)
        {
            "before": ["<leader>", "s", "f"],
            "commands": ["workbench.action.quickOpen"],
        },
        // "search text"
        {
            "before": ["<leader>", "s", "t"],
            "commands": ["workbench.view.search"],
        },
        // "quit"
        {
            "before": ["<leader>", "q"],
            "commands": ["workbench.action.closeActiveEditor"],
        },
        // "explorer" (opens file explorer)
        // *Note: Try to switch to "o, e" for "open explorer". This makes more sense with
        // VSCode workflow
        {
            "before": ["<leader>", "e"],
            "commands": ["workbench.files.action.showActiveFileInExplorer"],
        },
        // "terminal"
        {
            "before": ["<leader>", "t"],
            "commands": ["workbench.action.terminal.toggleTerminal"],
        },
        // "language rename"
        {
            "before": ["<leader>", "l", "r"],
            "commands": ["editor.action.rename"],
        },
        // "language refactor"
        {
            "before": ["<leader>", "l", "R"],
            "commands": ["editor.action.refactor"],
        },
        // "language refactor"
        {
            "before": ["<leader>", "l", "R"],
            "commands": ["editor.action.refactor"],
        },
        // "language autofix"
        {
            "before": ["<leader>", "l", "a"],
            "commands": ["editor.action.autoFix"],
        },
        {
            "before": ["<leader>", "l", "f"],
            "commands": ["editor.action.formatDocument"],
        },
        /* Primary Sidebar */
        // "Open git"
        {
            "before": ["<leader>", "o", "g"],
            "commands": ["workbench.view.scm", "workbench.scm.focus"],
        },
        // "Open search"
        {
            "before": ["<leader>", "o", "f"],
            "commands": ["workbench.view.search"],
        },
        // "Open explorer"
        {
            "before": ["<leader>", "o", "e"],
            "commands": ["workbench.view.explorer"],
        },
        // "Close"
        {
            "before": ["<leader>", "c"],
            "commands": ["workbench.action.closeSidebar"]
        },
        /* Auxiliary Sidebar */
        // "Open outline"
        {
            "before": ["<leader>", "O", "o"],
            "commands": ["outline.focus"]
        },
        // "Close" (auxiliary)
        {
            "before": ["<leader>", "C"],
            "commands": ["workbench.action.closeAuxiliaryBar"]
        },
        /* Git */
        // "Git stage file"
        {
            "before": ["<leader>", "g", "s", "f"],
            "commands": ["git.stage"]
        },
        // "Git [un]Stage all"
        {
            "before": ["<leader>", "g", "s", "a"],
            "commands": ["git.stageAll"]
        },
        {
            "before": ["<leader>", "g", "S", "f"],
            "commands": ["git.unstage"]
        },
        // "Git [un]Stage all"
        {
            "before": ["<leader>", "g", "S", "a"],
            "commands": ["git.unstageAll"]
        },
        // "Git commit file"
        {
            "before": ["<leader>", "g", "c", "f"],
            "commands": ["git.commit"],
        },
        // "Git commit all"
        {
            "before": ["<leader>", "g", "c", "a"],
            "commands": ["git.commitAll"],
        },
        // "Git commit staged"
        {
            "before": ["<leader>", "g", "c", "s"],
            "commands": ["git.commitStaged"],
        },
        // "Git commit Amend"
        {
            "before": ["<leader>", "g", "c", "A"],
            "commands": ["git.commitAmmend"],
        },
        // "Git discard" (discard changes in current file)
        {
            "before": ["<leader>", "g", "d"],
            "commands": ["git.clean"],
        },
        // "Git global discard" (discard all untracked working tree changes)
        {
            "before": ["<leader>", "g", "G", "d"],
            "commands": ["git.cleanAllUntracked"]
        },
        // "Git GLOBAL DISCARD" (nuke the working tree)
        {
            "before": ["<leader>", "g", "G", "D"],
            "commands": ["git.cleanAll"]
        },
        // "git push"
        {
            "before": ["<leader>", "g", "p"],
            "commands": ["git.pushTo"]
        },
        // "git RESET"
        {
            "before": ["<leader>", "g", "R"],
            "commands": ["gitLens.gitCommands.reset"]
        }
    ],
    "vim.visualModeKeyBindings": [
        // "selection comment block"
        {
            "before": ["<leader>", "/", "b"],
            "commands": ["editor.action.blockComment"],
        },
        // "selection comment line"
        {
            "before": ["<leader>", "/", "l"],
            "commands": ["editor.action.commentLine"],
        },
        // "selection cursor"
        {
            "before": ["<leader>", "c"],
            "commands": ["editor.action.insertCursorAtEndOfEachLineSelected"],
        },
        // "selection all" (adds a cursor on each token matching selection)
        {
            "before": ["<leader>", "a"],
            "commands": ["addCursorsAtSearchResults"],
        },
        // "selection indent"
        {
            "before": [ ">" ],
            "commands": [
                "editor.action.indentLines"
            ]
        },
        // "selection outdent"
        {
            "before": [ "<" ],
            "commands": [
                "editor.action.outdentLines"
            ]
        },
    ],
    "editor.minimap.enabled": false,
    "editor.tabSize": 2,
    "typescript.updateImportsOnFileMove.enabled": "never",
    "files.autoSave": "afterDelay",
    "workbench.iconTheme": "material-icon-theme",
}